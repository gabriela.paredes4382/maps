<br>
<h1>DIRECCION DE LA UTC</h1>
<img src="<?php echo base_url('assets/img/marcador.png'); ?>" alt="logo utc">
<br>
<div id="mapa1" style="width:100%; height:500px; border:2px solid black;"> <!-- Sin style no se vera el mapa-->
</div>
<script type="text/javascript">
  function initMap(){
    //creando una coordenada
    var coordenadaCentral=new google.maps.LatLng(-0.9176298851159721, -78.63297507638866);
    var miMapa=new google.maps.Map(document.getElementById("mapa1"),
      {
        center: coordenadaCentral,
        zoom:9,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }
    );
    //instanciando un objeto (marcadorUTC) desde la clase (MArker)
    var marcadorUTC= new google.maps.Marker({
      //instanciar un objeto de tipo coordenada
      position:new google.maps.LatLng(-0.9176298851159721,-78.63297507638866),
      map: miMapa,
      title: 'UTC Matriz',
      icon:'<?php echo base_url('assets/img/marcador.png'); ?>'
    });
    var marcadorSalache= new google.maps.Marker({
      //instanciar un objeto de tipo coordenada
      position:new google.maps.LatLng(-0.9968885404629667,-78.61866446000336),
      map: miMapa,
      title: 'UTC Campus Salache'
    });
    var marcadorPujili= new google.maps.Marker({
      //instanciar un objeto de tipo coordenada
      position:new google.maps.LatLng(-0.9556667198510903,-78.69578325804385),
      map: miMapa,
      title: 'UTC Pujili'
    });
    var marcadorMana= new google.maps.Marker({
      //instanciar un objeto de tipo coordenada
      position:new google.maps.LatLng(-0.9500142499100956,-79.22894983051687),
      map: miMapa,
      title: 'UTC Mana'
    });
  }
</script>
