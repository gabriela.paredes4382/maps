<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$this->load->view('header');//Cargando cabecera
		$this->load->view('welcome_message');//cargando Contenido
		$this->load->view('footer');//cargando Pie de pagina (footer)
	}
	public function universidad()
	{
		$this->load->view('header');//Cargando cabecera
		$this->load->view('universidad');//cargando Contenido
		$this->load->view('footer');//cargando Pie de pagina (footer)
	}
	public function ciudad(){
		$this->load->view('header');
		$this->load->view('ciudad');
		$this->load->view('footer');
	}
}
